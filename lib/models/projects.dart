import 'package:portfolio/content/project_content.dart';

class Projects{
  final String? title,description,animationMessage,projectType,projectLink,longContent;

  Projects({this.title,this.description,this.animationMessage,this.projectType,this.projectLink,this.longContent});
}

List<Projects> demoProjects=[

  Projects(
      title: "CREDAI National",
      animationMessage: "Event App",
      projectType: "Flutter",
      projectLink: "https://play.google.com/store/apps/details?id=com.credai.national&pcampaignid=web_share",
      longContent: ProjectContent.credai,
      description: "Confederation of Real Estate Developers' Associations of India (CREDAI) is the apex body of "
          "private Real Estate developers in India, established in 1999, with a vision of transforming the landscape "
          "of Indian Real Estate industry and a mandate to pursue the cause of Housing and Habitat."
  ),
  Projects(
      title: "Ksheer Sagar ERP",
      animationMessage: "Organization recipes and employee attendance tracking",
      projectType: "Flutter",
      longContent: ProjectContent.ksheerSagar,
      projectLink: "https://play.google.com/store/apps/details?id=com.ksheersagar.android",
      description: "KS ERP is a robust application designed to streamline and manage various business processes "
          "efficiently. Whether you are tracking employee attendance, managing orders, controlling inventory, or "
          "organizing recipes, KS ERP provides a comprehensive solution to enhance productivity and operational "
          "efficiency."
  ),
  Projects(
    title: "Cashpal",
    longContent: ProjectContent.cashPal,
    animationMessage: "Bill splitting and money sharing app",
    projectType: "Apple",
    projectLink: "https://apps.apple.com/in/app/cashpal-app/id6445887252.",
    description: "An app for bill splitting and money sharing between friends, in this project I learn to implement chat through socket and much more functionalities."
  ),
  Projects(
      title: "WagKart",
      longContent: ProjectContent.wagKart,
      animationMessage: "Pet walking service providing app",
      projectType: "Android",
      projectLink: "https://play.google.com/store/apps/details?id=com.wagkart.app",
      description: "Helping pet parents raise healthy and happy furbabies. "
          "Book professional dog walking services at a time of your convenience by downloading the app. "
          "Track your dog's walk and view past walk reports to stay updated of your dog's exercise routine."
  ),
  Projects(
      title: "Package Delivery",
      longContent: ProjectContent.package,
      animationMessage: "UAE based pickup and drop packages app",
      projectType: "Android",
      // projectLink: "https://play.google.com/store/apps/details?id=com.dripp.drippuser",
      description: "Need to send a package across the UAE? Our app makes it quick, easy, and secure! Whether it’s documents, gifts, or business deliveries, we connect you with trusted drivers for on-demand or scheduled pickups and drop-offs."
  ),
];