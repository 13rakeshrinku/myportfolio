class ProjectContent{

  static String credai="Confederation of Real Estate Developers' Associations of India (CREDAI) is the apex body of "
      "private Real Estate developers in India, established in 1999, with a vision of transforming the landscape "
      "of Indian Real Estate industry and a mandate to pursue the cause of Housing and Habitat. \n\nThrough the "
      "CREDAI National App, users will be able to access all ongoing and upcoming events such as NATCON, CONCLAVE, "
      "NEW INDIA SUMMIT and YOUTHCON, organized by various CREDAI chapters. Registered CREDAI members can also "
      "access informative sections like the E-Library, Knowledge Center, Notifications, and Policies.";

  static String ticTac="Tic Tac Toe is a classic two-player game where the players take turns placing their mark (an X or an O) on a 3x3 grid.\n\nThe objective of the game is to get three of your marks in a row, either horizontally, vertically, or diagonally.The game starts with an empty grid, and the first player (usually X) places their mark on any empty cell of the grid. Then, the second player (usually O) takes their turn and places their mark on an empty cell. The players continue taking turns until one of them gets three of their marks in a row or all the cells are filled. If all cells are filled and no one has won, the game ends in a draw.\n\n\nTic Tac Toe is a simple game that can be played by people of all ages, and it doesn't require any special equipment or setup. It's often used as a fun way to teach children about strategy, critical thinking, and problem-solving.";

  static String brainTest="Brain Test games applications are digital platforms that offer a variety of games and activities designed to stimulate and challenge the brain. These applications are developed with the goal of improving cognitive skills such as memory, attention, problem-solving, and reasoning.\n\nThe games in these applications can range from classic puzzles like Math Questions ,Sudoku and crossword puzzles to newer and more innovative games that require strategy and quick thinking. They often include timed challenges, leaderboards, and other features that encourage users to compete against each other and strive for improvement.\n\n\nBrain Test games applications are popular among people of all ages, but they are particularly popular among those who are interested in improving their cognitive abilities or maintaining their mental sharpness. They are also used by educators and therapists as a tool for enhancing learning and treating cognitive disorders.";

  static String ksheerSagar="KS ERP is a robust application designed to streamline and manage various business processes efficiently. Whether you are tracking employee attendance, managing orders, controlling inventory, or organizing recipes, KS ERP provides a comprehensive solution to enhance productivity and operational efficiency.";

  static String contactInfo="**If you have any question or concern, please do not hesitate to send a email with your contact information and I will response to you as soon as possible.**";

  static String cashPal="An app for bill splitting and money sharing between friends, in this project I learn to implement chat through socket and much more functionalities.";

  static String wagKart="Helping pet parents raise healthy and happy furbabies. "
      "Book professional dog walking services at a time of your convenience by downloading the app. "
      "Track your dog's walk and view past walk reports to stay updated of your dog's exercise routine.";

  static String package="Our UAE-based pick-and-drop package delivery app is designed to offer a seamless, reliable, "
      "and efficient way to send and receive packages across the country. Whether you need to send important documents,"
      " gifts, or business parcels, our platform connects you with a network of trusted drivers, "
      "ensuring safe and timely deliveries.\n\nWith a user-friendly interface, booking a pickup is "
      "quick and effortless. Simply enter the pickup and drop-off locations, choose the appropriate "
      "vehicle for your package size, and confirm the request. Real-time tracking allows users to "
      "monitor their package throughout the journey, providing complete transparency and peace of "
      "mind.\n\nSecurity and reliability are at the core of our service, with verified drivers ensuring "
      "that every delivery is handled with care. Flexible payment options, including cash and online payments, "
      "make transactions hassle-free. Whether for personal or business needs, our app offers an on-demand and "
      "scheduled delivery solution tailored to your convenience. Experience a new standard of package delivery "
      "in the UAE with speed, efficiency, and trust.";

  static String aboutMe="Hello, I am a skilled Flutter and Android developer with 3 years of experience in designing, developing, and deploying mobile applications. My expertise lies in creating high-quality and performant applications that provide an intuitive user experience.\n\nThroughout my career, I have worked with a variety of technologies and tools, including Flutter, Dart, Java, Kotlin, Android Studio, and Firebase. I have experience in building applications for a range of industries, including healthcare, finance, and e-commerce.\n\nI am a self-motivated and dedicated developer who thrives in a fast-paced and challenging environment. I have excellent communication skills and work well in a team environment, collaborating with designers, project managers, and other developers to deliver quality software products.\n\nIn my free time, I enjoy exploring new technologies and experimenting with different coding techniques to enhance my skills. I am passionate about developing innovative solutions and staying up-to-date with the latest trends in mobile development.\n\nIf you are looking for a skilled Flutter and Android developer who can deliver high-quality software solutions, please feel free to reach out to me. I am excited to hear about your project and how I can help bring your vision to life.";

}